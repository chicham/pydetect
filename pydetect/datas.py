import numpy as np
from voctools import utils

def latent_binary_problem(classifier, df, cls, win,
                          max_layer=15, step=3):
    cols = ['filename', 'name', 'xmin', 'ymin', 'xmax', 'ymax']
    if hasattr(df, 'difficult'):
        subsets = df.ix[df.difficult == 0, cols]
    else:
        subsets = df[cols]

    samples = np.empty(len(subsets), np.object)
    labels = np.zeros(len(subsets), np.int)

    for i, tup in enumerate(subsets.itertuples(index=False)):
        filename, name, xmin, ymin, xmax, ymax = tup
        img = utils.imread(filename)
        if name == cls:
            label, bbox = get_latent_positif(classifier, img,
                                            (xmin, ymin, xmax, ymax),
                                            win, step, max_layer)
            xmin, ymin, xmax, ymax = bbox
            labels[i] = label

        samples[i] = img[xmin:xmax, ymin:ymax].copy()

    # cond = np.logical_or(labels == 1, labels == 0)
    return labels, samples
