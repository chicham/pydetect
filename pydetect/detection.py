from voctools.detection import detect


def mixture_detection(image, mixture, wins, step, max_layer=5, max_scale=2):
    for i, enum in enumerate(zip(mixture, wins)):
        print "Classifier {0}".format(i)
        classifier, win = enum
        yield detect(image, classifier, win, step,
                                      max_layer, max_scale)

