from sklearn.pipeline import TransformerMixin, BaseEstimator
from fn import F, _
from skimage import feature, color
from scipy import misc, ndimage
from collections import deque
from scipy import linalg
from functools import partial
from .pyhog.pyhog import features_pedro
from sklearn import preprocessing, decomposition
from sklearn.mixture import GMM, DPGMM
from sklearn.covariance import OAS, EmpiricalCovariance
import numpy as np
from voctools import datas
from cytoolz import itertoolz, functoolz
import itertools
import math

hog = feature.hog
daisy = feature.daisy
local_binary_pattern = feature.local_binary_pattern
hist = np.histogramdd
round = F() << np.int << np.round

def decorrelation(samples):
    N = samples.shape[0]
    samples = samples.reshape(N, -1)
    mu = np.mean(samples, axis=0)
    precision = OAS().fit(samples).get_precision()
    # precision = EmpiricalCovariance().fit(samples).get_precision()
    return mu, precision

def decorrelate(samples, mu, invSigma):
    N = samples.shape[0]
    samples = samples.reshape(N, -1)
    return np.dot(invSigma, (samples - mu).T).T


def resize_preprocess(samples, win):
    process = F(misc.imresize, size=win)
    return np.array(map(process, samples))


def hog_preprocess(samples, sbin, scale=False, norm=False, get_win=datas.fz_win):
    # h, w = utils.get_median_shape(samples)
    h, w = get_win(samples)
    process = F() << np.ravel << F(features_pedro, sbin=sbin) << partial(misc.imresize, size=(h, w))
    samples = np.array(map(process, samples))
    if scale:
        return preprocessing.scale(samples)
    elif norm:
        return preprocessing.normalize(samples)
    else:
        return samples


def hist_preprocess(samples, bins, normed=True):
    def process(data):
        r, g, b = data[..., 0].ravel(),\
            data[..., 1].ravel(),\
            data[..., 2].ravel()
        h, _ = hist((r, g, b), bins=bins, normed=normed)
        return h.ravel()
    return np.array(map(process, samples))


def map_concatenate(f, datas):
    func = F() >> f >> _[np.newaxis, ...]
    return np.concatenate(map(func, datas))


class Decorrelater(BaseEstimator, TransformerMixin):

    def fit(self, samples, labels):
        neg = np.compress(labels, samples, axis=0)
        mu, invSigma = decorrelation(neg)
        self.mu = mu
        self.invSigma = invSigma

    def transform(self, samples):
        return decorrelate(samples, self.mu, self.invSigma)

    def fit_transform(self, samples, labels):
        self.fit(samples, labels)
        return self.transform(samples)


class MeanTransformer(BaseEstimator, TransformerMixin):

    def fit(self, datas, labels=None):
        return self

    def _one_transform(self, x):
        d = x.shape[-1]
        return x.reshape(-1, d).mean(axis=0)

    def transform(self, datas):
        return np.array(map(self._one_transform, datas))


class HogTransformer(BaseEstimator, TransformerMixin):

    def __init__(self, orientations=9, pixels_per_cell=(6, 6), cells_per_block=(2, 2), normalise=True):
        self.orientations = orientations
        self.pixels_per_cell = pixels_per_cell
        self.cells_per_block = cells_per_block
        self.normalise = normalise

    def transform(self, datas):
        func = F(hog,
                 orientations=self.orientations,
                 pixels_per_cell=self.pixels_per_cell,
                 cells_per_block=self.cells_per_block,
                 normalise=self.normalise)
        return map_concatenate(func, datas)

    def fit(self, datas, labels=None):
        return self


class GreyTransformer(TransformerMixin, BaseEstimator):

    def transform(self, datas):
        return map(color.rgb2grey, datas)

    def fit(self, datas, labels=None):
        return self


class ColorTransformer(TransformerMixin, BaseEstimator):

    def transform(self, datas):
        return map(color.gray2rgb, datas)

    def fit(self, datas, labels=None):
        return self


class PartsNormMaxTransformer(TransformerMixin, BaseEstimator):

    def __init__(self, extractor, n_parts=4):
        self.extractor = extractor
        self.n_parts = n_parts

    def _one_transform(self, data, n_parts):
        feat = self.extractor(data)
        data_x2 = misc.imresize(data, (data.shape[0]*2, data.shape[1]*2))
        feat_x2 = self.extractor(data_x2)
        size_parts = feat.shape[0]//4, feat.shape[1]//4
        shape = feat_x2.shape[0] - size_parts[0], feat_x2.shape[1] - size_parts[1]

        parts = deque()
        for p in np.arange(n_parts):
            norms = [(linalg.norm(feat_x2[i:i+size_parts[0], j:j+size_parts[1]]), i, j)
                     for i in xrange(0, shape[0], size_parts[0])
                     for j in xrange(0, shape[1], size_parts[1])]

            norms, i, j = zip(*norms)

            ind = np.argmax(norms)

            crop = np.s_[i[ind]:i[ind]+size_parts[0],
                         j[ind]:j[ind]+size_parts[1]]

            parts.append(feat_x2[crop].copy())
            feat_x2[crop] = 0

        return np.concatenate((feat.ravel(), np.asarray(parts).ravel()))

    def transform(self, datas):
        return map_concatenate(F(self._one_transform, n_parts=self.n_parts), datas)

    def fit(self, datas, labels=None):
        return self


class GridPartsTransformer(TransformerMixin, BaseEstimator):
    def __init__(self, config, feature):
        self.config = config
        self.feature = feature

    def transform(self, datas):
        parts = partial(map, partial(extract_spatial_grid_from_image, config=self.config))
        features = F() >> partial(map, self.feature) >> np.concatenate

        return map_concatenate(features, parts(datas))

    def fit(self, datas, labels=None):
        return self


class NaivePartsTransformer(TransformerMixin, BaseEstimator):

    def __init__(self, extractor, n_parts=6):
        self.n_parts = n_parts
        self.extractor = extractor

    def _one_transform(self, data, n_parts):
        feat = self.extractor(data)
        parts = deque()
        data_x2 = misc.imresize(data, (data.shape[0]*2, data.shape[1]*2))
        # data_x2 = ndimage.zoom(data, zoom)
        height, width = np.array(data_x2.shape[:2], np.float)
        ar = height / width
        n_parts_w = np.int(np.floor(np.sqrt(n_parts)))
        n_parts_h = np.int(n_parts // n_parts_w)
        if ar > 1:
            n_parts_h, n_parts_w = n_parts_w, n_parts_h
        size_part = (data_x2.shape[0]/(self.n_parts)), data_x2.shape[1] / (self.n_parts)
        start_w = np.int(width / (4 * n_parts_w))
        start_h = np.int(height / (4 * n_parts_h))
        for i in xrange(1, n_parts_h+1):
            for j in xrange(1, n_parts_w+1):
                part = self.extractor(data_x2[i*start_h:i*start_h+size_part[0],
                                              j*start_w:j*start_w+size_part[1]])
                parts.append(part)

        if len(parts) < self.n_parts:
            raise Exception('Error with parts placement')
        return np.concatenate((feat.ravel(), np.asarray(parts).ravel()))

    def transform(self, datas):
        return map_concatenate(F(self._one_transform, n_parts=self.n_parts), datas)

    def fit(self, datas, labels=None):
        return self


class UoCTTITransformer(TransformerMixin, BaseEstimator):

    def __init__(self, sbin=8):
        self.sbin = sbin

    def transform(self, datas):
        phog = F(features_pedro, sbin=self.sbin)
        return np.asarray(map(phog, datas))

    def fit(self, datas, labels=None):
        return self

    def fit_transform(self, datas, labels=None):
        return self.transform(datas)


class DaisyTransformer(BaseEstimator, TransformerMixin):

    def __init__(self, step=10, radius=6, rings=2, histograms=6, orientations=8,
                 normalization='daisy', sigmas=None, ring_radii=None):
        self.orientations = orientations
        self.step = step
        self.radius = radius
        self.rings = rings
        self.histograms = histograms
        self.normalization = normalization
        self.sigmas = sigmas
        self.ring_radii = ring_radii

    def transform(self, datas):
        func = F(daisy, step=self.step, radius=self.radius, rings=self.rings,
                 histograms=self.histograms, orientations=self.orientations,
                 normalization=self.normalization, sigmas=self.sigmas,
                 ring_radii=self.ring_radii) >> np.ravel
        return map_concatenate(func, datas)

    def fit(self, datas, labels=None):
        return self


class LBPTransformer(BaseEstimator, TransformerMixin):

    def __init__(self, radius=3, method='uniform'):
        self.points = 8 * radius
        self.radius = radius
        self.method = method

    def transform(self, datas):
        func = partial(lbp_hist, P=self.points, R=self.radius, method=self.method)

        return map_concatenate(func, datas)

    def fit(self, datas, labels=None):
        return self


class HistLBPTransformer(BaseEstimator, TransformerMixin):

    def __init__(self, radius=3, method='uniform'):
        self.points = 8 * radius
        self.radius = radius
        self.method = method
        self.shape = (4*radius, 4*radius, self.points + 1)

    def transform(self, datas):
        feat = partial(lbp_hist, P=self.points, R=self.radius, method=self.method)
        func = partial(patch_feature, feature=feat, patch_size=self.shape) 

        return map_concatenate(func, datas)

    def fit(self, datas, labels=None):
        return self


class HistogramTransformer(BaseEstimator, TransformerMixin):

    def __init__(self, nbins=64):
        self.nbins = nbins

    def transform(self, datas):

        def func(data):
            h, _ = hist(data.ravel(), bins=self.nbins)
            return np.asarray(h.ravel(), dtype=np.float)

        return map_concatenate(func, datas)

    def fit(self, datas, labels=None):
        return self


class ColorHistogramTransformer(BaseEstimator, TransformerMixin):

    def __init__(self, nbins=8):
        self.nbins = nbins

    def transform(self, datas):

        def func(data):
            r, g, b = data[..., 0].ravel(),\
                data[..., 1].ravel(),\
                data[..., 2].ravel()
            h, _ = hist((r, g, b), bins=(self.nbins, self.nbins, self.nbins))
            return np.asarray(h.ravel(), dtype=np.float)

        return map_concatenate(func, datas)

    def fit(self, datas, labels=None):
        return self


class ResizeTransformer(TransformerMixin):

    def __init__(self, shape):
        self.shape = shape

    def transform(self, samples):
        resize = F(misc.imresize, size=self.shape)
        return map(resize, samples)


class RavelTransformer(TransformerMixin, BaseEstimator):

    def transform(self, samples):
        return np.array(map(np.ravel, samples))

    def fit(self, samples, labels):
        return self

    def fit_transform(self, samples, labels=None):
        return self.transform(samples)


class PreprocessTransformer(TransformerMixin, BaseEstimator):

    def __init__(self, func):
        self.func = func

    def transform(self, samples):
        return map(self.func, samples)

    def fit(self, samples, labels):
        return self


def fv_component(x, means, precisions, sigmas):
    """
    Compute component p_i of the GMM
    """

    x_ = x - means
    num = -.5 * np.dot(np.dot(x_.T, precisions), x_)

    return math.exp(num) / (math.pow(2*np.pi, x.shape[-1] / 2.) * sigmas)


def d_mu(x, gamma, mean, covar):
    return (gamma[:, np.newaxis] * (x - mean) / covar).sum(axis=0)


def d_sigma(x, gamma, mean, covar):
    return (gamma[:, np.newaxis] * (np.power(x - mean / covar, 2)) - 1. ).sum(axis=0)


def f_mu(T, weight):
    return 1. / (T * math.sqrt(weight))


class FisherVector(GMM):
    def __init__(self, n_components=1, random_state=None, thresh=1e-2, min_covar=1e-3, n_iter=10, n_init=1, params='wmc', init_params='wmc'):
        super(FisherVector, self).__init__(n_components=n_components, random_state=random_state, thresh=thresh, min_covar=min_covar,n_iter = n_iter, covariance_type='diag', n_init=n_init, params=params, init_params=init_params)
        self.precisions_ = np.empty((self.n_components, self.n_components))
        self.sigmas_ = np.empty((self.n_components, self.n_components))


    def fit(self, X, y=None):

        self.n_dim = X[0].shape[-1]
        self.two_pi_pow_d = math.pow(2*np.pi, self.n_dim / 2.)
        # X_ = preprocessing.normalize(np.concatenate([xx.reshape(-1, self.n_dim) for xx in X]))
        X_ = np.concatenate([xx.reshape(-1, self.n_dim) for xx in X])

        super(FisherVector, self).fit(X_)

        # Order can change so we order them descending
        indices = np.argsort(self.weights_)
        self.means_ = self.means_[indices]
        self.covars_ = self.covars_[indices]
        self.weights_ = self.weights_[indices]
        self.precisions_ = np.array([np.diag(1. / self.covars_[i]) for i in xrange(self.n_components)])
        self.sigmas_ = np.array([math.sqrt(linalg.det(np.diag(self.covars_[i]))) for i in xrange(self.n_components)])
        return self

    
    def occupancy(self, x):
        gamma = np.array([weight*fv_component(x, mean, precision, sigma) for mean, precision, sigma, weight in zip(self.means_, self.precisions_, self.sigmas_, self.weights_)])
        return gamma / gamma.sum()


    def feature_map(self, x_t):
        """
        For all features of one image
        :x_t list of features
        """

        d = x_t.shape[-1]
        x_t = x_t.reshape(-1, d)
        T = x_t.shape[0]
        feature = np.empty(2 * d * self.n_components, np.float)

        gamma = np.array([self.occupancy(x) for x in x_t])

        fv_populate(feature, x_t, self.weights_, self.means_, self.covars_, gamma)

        # return feature / linalg.norm(feature)
        return feature


    def transform(self, X, y=None):
        return np.array(map(self.feature_map, X))

def fv_populate(feat, x_t, weights, means, covars, gamma):
    T, d = x_t.shape
    n_components = weights.shape[0]
    start = 0
    stop=start+d
    x_ = np.empty(x_t.shape, np.float)

    for w, m, c, g in zip(weights, means, covars, gamma.T):
        x_[:] = (x_t - m) / c
        feat[start:stop] = 1. / (math.sqrt(w)) * (g[:, np.newaxis] * x_).mean(axis=0)
        start, stop = stop, stop+d
        feat[start:stop] = 1. / (math.sqrt(2*w)) * (g[:, np.newaxis] * (np.power(x_, 2) - 1)).mean(axis=0)


def _extract_spatial_grid(template, config, interpolator, shape=None):
    template = template.reshape(shape)
    # template_x2 = ndimage.zoom(template, (2, 2, 1), mode='nearest', order=1)
    template_x2 = interpolator(template)
    new_shape = template_x2.shape
    step_h = np.int(new_shape[0] / config[0])
    step_w = np.int(new_shape[1] / config[1])
    return [template_x2[i:i+step_h, j:j+step_w, ...]
            for i in xrange(0, new_shape[0]-step_h+1, step_h)
            for j in xrange(0, new_shape[1]-step_w+1, step_w)]


def extract_spatial_grid(template, config, shape=None):
    zoom = (2, 2, 1)
    if len(shape) == 2:
        zoom = (2,2)
    interpolator = partial(ndimage.zoom, zoom=zoom, mode='nearest', order=1)
    return _extract_spatial_grid(template, config, interpolator, shape)


def extract_spatial_grid_from_image(template, config, shape=None):
    shape = template.shape
    new_size = (shape[0]*2, shape[1]*2)
    if len(template.shape) == 3:
        new_size = (shape[0]*2, shape[1]*2, shape[2])
    interpolator = partial(misc.imresize, size=new_size)
    return _extract_spatial_grid(template, config, interpolator, shape)


def lbp_hist(sample, P, R, method):
        func = partial(local_binary_pattern, P=P, R=R,
                       method=method)
        lbp = func(sample)
        n_bins = P + 1
        hist, _ = np.histogram(lbp, normed=True, bins=n_bins)
        return hist


def patches(window, shape, pad_x=0, pad_y=0):
    return ((i, j, i+window[0], j+window[1])
            for i in xrange(0, shape[0]-pad_x, window[0])
            for j in xrange(0, shape[1]-pad_y, window[1]))


def patch_feature(img, feature, patch_size):
    shape = img.shape
    out_shape = round(float(shape[0]) / patch_size[0]), round(float(shape[1]) / patch_size[1]), patch_size[2]
    # may be issue with empty
    out = np.zeros(out_shape, np.float)

    for i in xrange(out_shape[0]):
        for j in xrange(out_shape[1]):
            u = i * patch_size[0]
            v = j * patch_size[1]
            out[i , j, :] = feature(img[u:u+patch_size[0], v:v+patch_size[1]])

    return out

def sparse_signature(sample, dico, patch_size, alpha=0.5):

    pad_x, pad_y = sample.shape[0] % patch_size[0],\
            sample.shape[1] % patch_size[1]

    if pad_x > 0:
        sample = padding(sample, pad_x)
    if pad_x > 0:
        T = F(np.rollaxis, axis=1)
        sample = T(padding(T(sample), pad_y))

    shape = sample.shape

    if len(patch_size) == 3:
        h,w,z = patch_size
        patches = sample.reshape(-1, h * w * z)
    if len(patch_size) == 2:
        h, w = patch_size
        patches = sample.reshape(-1, h * w)

    patches = patches.astype(np.float)
    patches = preprocessing.scale(patches)

    codes = decomposition.sparse_encode(patches, dico)
    out_shape = shape[0] / patch_size[0], shape[1] / patch_size[1]
    codes = codes.reshape(out_shape[0], out_shape[1], -1)
    #TODO Finir !!



def padding(sample, pad):
    dp = pad / 2
    if not dp % 2:
        return sample[dp:-dp]
    else:
        return sample[dp:-(dp+1)]


# def hog_picture(feature, size):
#     bim1 = np.zeros(bs, bs)
#     r_bs = np.round(bs/2)
#     bim1[:, r_bs: r_bs +1]

    
