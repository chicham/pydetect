import fiona
import rasterio
import numpy as np
from path import path
from fn import F
from shapely import geometry
from pandas import DataFrame


def load(raster, shapefiles, offset=None):
    names = ('file', 'name', 'xmin', 'ymin', 'xmax', 'ymax')
    formats = ('a128', 'a64', np.int, np.int, np.int, np.int)
    rows = np.empty((0,), dtype={'names': names, 'formats': formats})

    for sample in samples_from_shapefiles(raster, shapefiles, offset):
        rows = np.hstack((rows, np.array(sample, dtype={'names': names, 'formats': formats})))
    return DataFrame(rows)


def to_pixel(trans, coord):
    return int((coord[0] - trans[0]) / trans[1]),\
        int((trans[3] - coord[1]) / trans[1])


def samples_from_shapefiles(raster_name, shapefiles, offset=None):
    with rasterio.open(path(raster_name).expand(), 'r') as raster:
        f = F(to_pixel, trans=raster.transform)
        for shapefile in shapefiles:
            name = shapefile.basename().stripext().split('_')[0]
            with fiona.open(path(shapefile).expand(), 'r') as source:
                for feat in source:
                    if feat['geometry']['type'] is 'Polygon':
                        for coord in feat['geometry']['coordinates']:
                            bbox = geometry.asPolygon(coord).bounds

                            ymin, xmin = f(coord=(bbox[0], bbox[3]))
                            ymax, xmax = f(coord=(bbox[2], bbox[1]))

                            if offset is not None:
                                xmin = xmin - offset
                                ymin = ymin - offset
                                xmax = xmax + offset
                                ymax = ymax + offset

                            if xmax - xmin > 0 and ymax - ymin > 0:
                                yield raster.name, name, xmin, ymin, xmax, ymax
