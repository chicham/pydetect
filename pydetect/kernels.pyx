import cython
import numpy as np

# Intersection d'histogrammes, par définition les bins sont > 0
@cython.ccall
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.returns(cython.float)
def intersection(X, Y):
    return np.sum(np.minimum(X, Y))


eps = 10.e-5

@cython.ccall
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.locals(N=cython.int, i=cython.int, j=cython.int)
def libsvm_intersection(X, Y):
    if X.ndim !=2 or Y.ndim != 2:
        raise Exception('Not a matrix')
    if X.size != Y.size:
        raise Exception('Dimension mismatch')
    N = X.shape[0]
    res = np.empty((N, N), np.float)

    for i in np.arange(N):
        for j in np.arange(i, N):
            k = intersection(X[i], Y[j])
            res[i, j] = k
            res[j, i] = k

    return res
