import numpy as np
from sklearn import cross_validation, metrics, base
from sklearn.base import BaseEstimator
from sklearn.linear_model import SGDClassifier, LogisticRegression
from sklearn.isotonic import IsotonicRegression
from sklearn.svm import LinearSVC
from sklearn.utils import atleast2d_or_csr
from sklearn.utils.extmath import safe_sparse_dot
from sklearn import preprocessing
from sklearn.cluster import spectral_clustering, KMeans, MeanShift, DBSCAN, Ward, estimate_bandwidth
from sklearn.manifold import Isomap, SpectralEmbedding, LocallyLinearEmbedding
from sklearn.pipeline import Pipeline
from sklearn.grid_search import RandomizedSearchCV, GridSearchCV
from sklearn.random_projection import johnson_lindenstrauss_min_dim
from sklearn.feature_selection import SelectKBest, f_classif
from scipy import linalg
from collections import deque
from . import features
from voctools import utils
from scipy import stats
from cytoolz import itertoolz
import itertools


def split_kmeans(samples, n_clusters, labels=False, min_samples=1):
    if samples.ndim == 1:
        samples = samples.reshape(-1, 1)
    else:
        samples = np.array(map(np.ravel, samples))
    kmeans = KMeans(n_clusters=n_clusters)
    return kmeans.fit_predict(samples)


def split_meanshift(samples, **kwargs):
    if samples.ndim == 1:
        samples = samples.reshape(-1, 1)
    else:
        samples = np.array(map(np.ravel, samples))

    kwargs.setdefault('cluster_all', False)
    kwargs.setdefault('bandwidth', estimate_bandwidth(samples, quantile=0.2))
    mshift = MeanShift(**kwargs)
    return mshift.fit_predict(samples)


def split_dbscan(samples, percentile=.5, **kwargs):
    if samples.ndim == 1:
        samples = samples.reshape(-1, 1)
    else:
        samples = np.array(map(np.ravel, samples))
    eps = estimate_bandwidth(samples, quantile=percentile)
    kwargs.setdefault('eps', eps)
    dbscan = DBSCAN(**kwargs)
    return dbscan.fit_predict(samples)


def split_spectral(samples,n_clusters, quantile=0.3, gamma='auto', eigen_tol=0.0, assign_labels='discretize'):

    if samples.ndim == 1:
        samples = samples.reshape(-1, 1)
    else:
        samples = np.array(map(np.ravel, samples))

    samples = preprocessing.normalize(samples)
    N, d = samples.shape

    if gamma is 'auto':
        gamma = estimate_bandwidth(samples, quantile=quantile)

    # eps = [0.1, 0.2, 0.3, 0.4, 0.5]
    # indx, = np.where(johnson_lindenstrauss_min_dim(N, eps=eps) < d)
    # if indx.size == 0:
    #     n_components = n_clusters
    # else:
    #     n_components = indx[0]

    affinity = metrics.pairwise_kernels(samples, metric='rbf', gamma=gamma)
    return spectral_clustering(affinity,
                               n_clusters=n_clusters, 
                               eigen_solver='arpack',
                               assign_labels=assign_labels,
                               n_components=None)


def split_hierarchical(samples, **kwargs):
    if samples.ndim == 1:
        samples = samples.reshape(-1, 1)
    else:
        samples = np.array(map(np.ravel, samples))

    ward = Ward(**kwargs)
    return ward.fit_predict(samples)


def dimension_transform(samples):
    return np.asarray([sample.shape for sample in samples])


def ar_transform(samples):
    height, width = zip(*(sample.shape[:2] for sample in samples))
    height = np.array(height, dtype=np.float)
    width = np.array(width, dtype=np.float)
    return np.log(height / width)


def isomap_transform(samples, n_neighbors, n_components):
    return Isomap(n_neighbors=n_neighbors, n_components=n_components).fit_transform(samples)


def spectral_embedding_transform(samples, **kwargs):
    return SpectralEmbedding(**kwargs).fit_transform(samples)


def lle_transform(samples, n_components, n_neighbors, method):
    return LocallyLinearEmbedding(n_neighbors=n_neighbors, n_components=n_components, method=method, eigen_solver='dense').fit_transform(samples)


def split_by_ar(samples, labels, k):
    pos = np.compress(labels, samples)
    height, width = zip(*(sample.shape[:2] for sample in pos))
    height = np.array(height, dtype=np.float)
    width = np.array(width, dtype=np.float)
    aspects = height / width
    aspects = np.sort(aspects)
    nb = np.float(len(pos))
    threshs = np.zeros(k+1)

    for i in xrange(k+1):
        j = int(np.ceil(i*(nb-1)/k))
        if j > len(pos):
            threshs[i] = np.inf
        else:
            threshs[i] = aspects[j]

    aspects = (height / width)
    for i in xrange(k):
        cond = np.logical_and(aspects >= threshs[i],
                              aspects < threshs[i+1])
        yield np.where(cond)[0]


def empiprical_probabilities(samples, labels, bin_size=0.08):
    samples = samples.ravel()
    s_min = samples.min()
    samples = (samples - s_min) / (samples.max() - s_min)
    labels = labels.astype(np.float)
    bins = np.arange(0, 1, bin_size)
    indx = np.digitize(samples, bins) - 1
    unique_indx = np.unique(indx)
    probas = np.zeros(bins.size, np.float)

    for ind_bin in unique_indx:
        bin_labels = labels[ind_bin == indx]
        probas[ind_bin] = bin_labels.sum() / bin_labels.size
        print probas[ind_bin]

    return probas[indx]


def calibration(classifier, labels, samples, method='logistic', scoring='average_precision'):

    sc = ScoreClassifier(classifier)
    samples = sc.transform(samples)

    print ""
    print "Calibration"
    print "Number of val  samples: {}".format(len(samples))
    print "Number of positif samples: {}".format(labels.sum())

    if method == 'logistic':
        cali = LogisticCalibration(penalty='l2',
                                   class_weight='auto')

        gridcv = GridSearchCV(cali,
                                    {'C': 10.**-np.arange(-4, 6)},
                                  cv=cross_validation.KFold(labels.size, n_folds=5, shuffle=True),
                                    scoring=scoring,
                                    verbose=1,
                                    n_jobs=-1)


        gridcv.fit(samples, labels)
        utils.gridsearch_report(gridcv.grid_scores_)
        cali = gridcv.best_estimator_

    elif method == 'isotonic':
        cali = IsotonicCalibration().fit(samples.ravel(), labels)
    else:
        cali = MinMaxCalibration().fit(samples, labels)


    pipeline = Pipeline([('clf', sc),
                         ('calibration', cali)])

    return pipeline

class MinMaxCalibration(BaseEstimator):
    def fit(self, samples, labels=None):
        self.min = samples.min()
        self.max = samples.max()
        return 

    def transform(self, samples, labels=None):
        return (samples - self.min) / (self.max - self.min)

    def decision_function(self, samples):
        return (samples - self.min) / (self.max - self.min)


class IsotonicCalibration(IsotonicRegression):

    def decision_function(self, samples):
        samples = np.clip(samples.ravel(), self.X_.min(), self.X_.max())
        return self.transform(samples)

def sigmoid(X):
    X *= -1
    np.exp(X, X)
    X += 1
    np.reciprocal(X, X)
    return X.ravel()
 

class LogisticCalibration(LogisticRegression):

    def decision_function(self, samples):
        return sigmoid(super(LogisticRegression, self).decision_function(samples))


class ScoreClassifier(BaseEstimator):

    def __init__(self, model):
        self.model = model

    def transform(self, samples):
        return self.model.decision_function(samples).reshape((-1, 1))

    def fit(self, X, y=None):
        return self

    def decision_function(self, samples):
        return self.model.decision_function(samples)



class SpatialGridClassifier(SGDClassifier):

    def __init__(self, config, shape, loss='hinge', penalty='l2', alpha=0.0001, l1_ratio=0.15, fit_intercept=True, n_iter=5, shuffle=False, verbose=0, epsilon=0.1, n_jobs=1, random_state=None, learning_rate='optimal', eta0=0.0, power_t=0.5, class_weight=None, warm_start=False, rho=None, seed=None):
        super(SGDClassifier, self).__init__(loss=loss, penalty=penalty, alpha=alpha, l1_ratio=l1_ratio, fit_intercept=fit_intercept, n_iter=n_iter, shuffle=shuffle, verbose=verbose, epsilon=epsilon, n_jobs=n_jobs, random_state=random_state, learning_rate=learning_rate, eta0=eta0, power_t=power_t, class_weight=class_weight, warm_start=warm_start, rho=rho, seed=seed)
        self.config = config
        self.shape = shape

    def fit(self, samples, labels=None):
        super(SGDClassifier, self).fit(samples, labels)
        self.template_ = self._extract(self.coef_)
        return self

    def decision_function(self, samples):
        samples = np.concatenate(map(self._extract, samples))
        samples = atleast2d_or_csr(samples)
        scores = safe_sparse_dot(samples, self.template_.T, dense_output=True)\
            + self.intercept_
        return scores.ravel()

        # return super(SGDClassifier, self).decision_function(np.concatenate(map(self._extract, samples)))

    def _extract(self, sample):
        parts = features.extract_spatial_grid(sample,
                                              self.config,
                                              self.shape)
        coef = np.concatenate(map(lambda x: x.ravel(), parts)).reshape((1, -1))
        return np.hstack((sample.reshape((1, -1)), coef))


class HyperplaneClassifier(BaseEstimator):
    def __init__(self, coef, intercept):
        self.coef_ = coef
        self.intercept_ = intercept

    def decision_function(self, samples):
        samples = atleast2d_or_csr(samples)
        scores = safe_sparse_dot(samples, self.coef_.T, dense_output=True) + self.intercept_
        return scores.ravel()


class BandSelection(BaseEstimator):
    def __init__(self, n_bands=3):
        self.n_bands = 3
        self.indices = None

    def fit(self, X, y):
        datas = [(xxx, yy) for xxx in xx.reshape(-1, xx.shape[-1]) for xx, yy in zip(X, y)]
        X_, y_ = zip(*datas)
        X_ = np.array(X_)
        y_ = np.array(y_)
        # Anova
        self.mask = SelectKBest(f_classif, k=self.n_bands).fit(X_, y_).get_support()
        return self

    def transform(self, X, y=None):
        if self.mask is not None:
            return X[..., self.mask]
        else:
            raise Exception('Estimator not fitted')



def sqrtm(arr):
    return linalg.sqrtm(arr).real


def multitask_learners(samples, T_labels, epsilon):
    N, d = samples.shape
    T, _  = T_labels.shape
    before_scores = deque()

    classifiers = deque()
    for labels in T_labels:
        gridcv = GridSearchCV(LinearSVC(dual=False, class_weight='auto'),
                            param_grid={'C': 10.**np.arange(-5,5)},
                            scoring='average_precision',
                            n_jobs=4,
                            verbose=0)
        gridcv.fit(samples, labels)
        clf = gridcv.best_estimator_
        before_scores.append(cross_validation.cross_val_score(clf, samples, labels, scoring='average_precision'))
        classifiers.append(clf)


    W = np.zeros((d, T), np.float)
    W_ = np.empty((d, T), np.float)
    D = np.eye(d, d) * 1./(d+epsilon)
    iter_samples = np.empty((N, d), np.float)

    err = 1./epsilon
    print "MTL"
    while err > epsilon:
        iter_samples = sqrtm(D).dot(samples.T).T
        W_ = W.copy()

        for t, tup in enumerate(zip(classifiers, T_labels)):
            model, labels = tup
            m = base.clone(model)
            m.fit(iter_samples, labels)
            W[:, t] = m.coef_.ravel()

        W = np.dot(sqrtm(D), W)
        WWT = W.dot(W.T)
        WWT += epsilon*np.eye(d,d)
        sqrtWWT = sqrtm(WWT)
        D = sqrtWWT / np.trace(sqrtWWT)
        err = linalg.norm(W - W_) 
        print err

    print ""
    for c, labels, b_scores, w in zip(classifiers, T_labels, before_scores, W.T):
        scores = c.decision_function(samples)
        ap = metrics.average_precision_score(labels, scores)
        print "Average precision scores:"
        print "Number of positive samples {:d}".format(labels.sum())
        print "Before multitask: {0:.3f}".format(ap)
        c = HyperplaneClassifier(w, c.intercept_)
        scores = c.decision_function(samples)
        ap = metrics.average_precision_score(labels, scores)
        print "After multitask: {0:.3f} ".format(ap)
        print ""

    return classifiers, D

