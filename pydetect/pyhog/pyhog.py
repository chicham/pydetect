import pdb
import features_pedro_py
import numpy as np

def features_pedro(img, sbin):
    imgf = np.asarray(img, dtype=np.float, order='F')
    # img = img.astype(np.float)
    # imgf = img.copy('F')
    hogf = features_pedro_py.process(imgf, sbin)
    return hogf
