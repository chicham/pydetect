import numpy as np
import math
import cython
from numpy import linalg, random


@cython.ccall
@cython.locals(m=cython.int, d=cython.int, t=cython.int, i=cython.int, tau=cython.int, loss_t=cython.double, lambda_reg=cython.double, n_iter=cython.int)
def pegasos(samples, labels, lambda_reg, n_iter,k=5) :
    
    #Verify labels
    labels[labels == 0] = -1
    samples = np.column_stack((samples, np.repeat(1, samples.shape[0])))
    m, d = samples.shape
    w = np.zeros((d,1))
    n_iter+=1
    b=1

    for i in xrange(1, n_iter):
        for tau in xrange(m):
            print w
            label_sample = labels[tau]*samples[tau]
            margin = np.dot(label_sample, w) 
            eta = lambda_reg*i
            w *= (1.-1./eta)
            # Learn biais
            # b += labels[tau] * (1./eta*m)
            if margin < 1:
                w += (1./eta)*label_sample.reshape(-1,1)
            # w = np.minimum(1., (1/math.sqrt(lambda_reg)) / linalg.norm(w))* w

    w = w.ravel()
    print b
    print w
    return w, b

