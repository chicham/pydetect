#!/usr/bin/env python
"""
setup.py
"""

from setuptools import setup, find_packages
from Cython.Build import cythonize

setup(name='pydetect',
      version='0.1.1dev',
      author='Hicham Randrianarivo',
      author_email='hicham.randrianarivo@onera.fr',
      packages=find_packages(),
      install_requires=['scikit-image', 'scikit-learn'],
      # dependency_links=['git+git://github.com/chicham/voctools.git#egg=voctools'],
      ext_modules=cythonize([ 'pydetect/*.pyx']))

