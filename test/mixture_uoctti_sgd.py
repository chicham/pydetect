# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>


from path import path
devkit = path('~/Documents/works/voc-dpm/cachedir/VOC7/VOCdevkit')
cls = 'cars'

from voctools import datas

labels, samples = zip(*datas.binary_problem(cls, 'trainval.txt', devkit))

print "{0} samples".format(len(samples))
print "{0} positives samples".format(sum(labels))

# <codecell>

from fn import iters
import numpy as np


def rs_image(im):
    return im[:, :, :3]


def preprocessing(samples):
    return map(rs_image, samples)


from sklearn.pipeline import Pipeline
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from pydetect.learners import KMeansMixtureOfClassifiers
from pydetect.features import UoCTTITransformer

classifier = Pipeline([(('hog'), UoCTTITransformer()),
                       ('scaler', StandardScaler()),
                       (('learner'), SGDClassifier(shuffle=True,
                        n_iter=10 ** 6 / len(samples)))])

kmeans_feature = np.frompyfunc(lambda x: (np.float(x.shape[0]) / x.shape[1]), 1, 1)

mixture = KMeansMixtureOfClassifiers(feature=kmeans_feature)

mixture_params = {'classifier__learner__loss': ('hinge', 'huber'),
                  'classifier__learner__penalty': ('l1', 'l2', 'elasticnet'),
                  'classifier__learner__alpha': 10.**-np.r_[1:7:100j],
                  'classifier__learner__power_t': np.r_[.1:1:100j],
                  'nb_clusters': (2, 3, 4, 5, 6, 7, 8)}

from sklearn.grid_search import RandomizedSearchCV

gs = RandomizedSearchCV(mixture, mixture_params, n_iter=5, scoring='f1')

gs.fit(list(preprocessing(samples)), labels)

mixture = gs.best_estimator_

print mixture

print "Learning done"

scores = mixture.decision_function(iters.compress(preprocessing(samples), labels))
thresh = np.percentile(mixture.decision_function(
    iters.compress(preprocessing(samples), labels)), 20)
print thresh

from voctools import evaluation
from pydetect import detection
from scipy import misc

img_test = misc.imread(
    path('/home/hicham/Dropbox/Documents/christchurch/images/1-0005-0004.tif'))

img_test = img_test[:, :, :3]

boxes = tuple(detection.mixture_detection(img_test, mixture, 5, max_layer=5, max_scale=2))

import pylab
from pylab import gcf, gca

pylab.clf()
gcf().set_size_inches((20, 40))
pylab.imshow(img_test)

from voctools.detection import nms, plot_detection
# f_boxes = [(bndbox, score) for bndbox, score in boxes if score > thresh]
nms_inds = nms(boxes, 0.1)
nms_boxes = [boxes[ii] for ii in nms_inds]
bndboxes, scores = zip(*nms_boxes)
plot_detection(gca(), bndboxes)

# <codecell>

precision, recall = evaluation.detection(cls, nms_boxes, devkit, thresh=0.3)

#area = auc(recall, precision)
ap = evaluation.average_precision(precision, recall)

print ap

import pylab as pl
pl.clf()
gcf().set_size_inches((6, 5))
pl.plot(recall, precision, label='Precision-Recall curve')
pl.xlabel('Recall')
pl.ylabel('Precision')
pl.ylim([0.0, 1.05])
pl.xlim([0.0, 1.0])
pl.title('Precision-Recall example: AP={0}'.format(ap))
pl.legend(loc="upper right")
pl.show()
