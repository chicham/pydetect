print(__doc__)

import numpy as np
import pylab as pl
from sklearn import linear_model, datasets, preprocessing
from pydetect.svm import pegasos

# we create 40 separable points
iris = datasets.load_iris()
X = preprocessing.scale(iris.data[:, :2])
Y = iris.target

# fit the model
clf = linear_model.SGDClassifier(n_iter=150)
clf.fit(X, Y)

# get the separating hyperplane
w = clf.coef_[0]
w2, b2 = pegasos(X, Y, lambda_reg=1, n_iter=10)

a = -w[0] / w[1]
xx = np.linspace(-3, 3)
yy = a * xx - (clf.intercept_[0]) / w[1]
print clf.intercept_[0]
a2 = -w2[0] / w2[1]
yy2 = a2 * xx - b2 / w[1]

# plot the parallels to the separating hyperplane that pass through the
# support vectors
# b = clf.support_vectors_[0]
# yy_down = a * xx + (b[1] - a * b[0])
# b = clf.support_vectors_[-1]
# yy_up = a * xx + (b[1] - a * b[0])

# plot the line, the points, and the nearest vectors to the plane
pl.plot(xx, yy, 'r-')
pl.plot(xx, yy2, 'b-')
# pl.plot(xx, yy_down, 'k--')
# pl.plot(xx, yy_up, 'k--')

# pl.scatter(clf.support_vectors_[:, 0], clf.support_vectors_[:, 1],
           # s=80, facecolors='none')
pl.scatter(X[:, 0], X[:, 1], c=Y, cmap=pl.cm.Paired)

pl.axis('tight')
pl.show()
