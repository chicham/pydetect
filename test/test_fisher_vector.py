from sklearn.preprocessing import Normalizer
from pydetect.features import FisherVector
from cytoolz import functoolz
from sklearn.grid_search import GridSearchCV
from sklearn.preprocessing import Normalizer
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC
import rasterio
import numpy as np

def imread(name):
    with rasterio.drivers():
        with rasterio.open(name) as src:
            bands = map(functoolz.compose(np.atleast_3d, src.read_band),
                        src.indexes)
            return np.concatenate(bands, axis=2)



def fit(X, y=None):
    # pipeline = Pipeline([('norm', Normalizer()),
    #                      ('fisher', FisherVector(n_components=8)),
    #                      ('clf', LinearSVC()),
    #     ])

    # cv = GridSearchCV(pipeline, {'norm__norm': ('l2', 'l1')}, n_jobs=-1, verbose=1)

    # return cv.fit(X, y).best_estimator_
    return FisherVector(n_components=8).fit(X)


def transform(clf, X):
    return clf.transform(X)


if __name__ == '__main__':
    img = imread('/home/hicham/Dropbox/Documents/igarss2014/rasters/train/TelopsDatasetCityLWIR.img')
    X = img.reshape(-1, 84)[:10000]
    y = np.random.randint(0, 2, X.shape[0])
    clf = fit(X, y)

    t = transform(clf, X)
    print t.shape
